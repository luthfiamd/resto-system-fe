import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { Container } from 'reactstrap'

import "../App.css";


class Response extends String { json = () => JSON.parse(this) }

function mockFetch(url, { body }) {
    const { email, password } = body
    // output auth taro sini
    if (email !== 'admin@admin.com' || password !== 'password')
        return Promise.reject("Email atau password salah!")

    return Promise.resolve(
        new Response(
            JSON.stringify({ accessToken: 'ini-token' })
        )
    )
}

class Login extends Component {
    state = { email: null, password: null, id: null }
    set = name => event => this.setState({ [name]: event.target.value })
    handleSubmit = event => {
        event.preventDefault()
        const { email, password, id } = this.state
        const { history } = this.props
        mockFetch('http://api.expample.com/v1/auth/login', {
            body: { email, password },
            params: { id }
        })
            .then(response => response.json())
            .then(json => {
                localStorage.setItem('token', json.accessToken)
                history.push('/' + id)
            })
            .catch(err => { alert(err) }
            )
    }

    render() {
        return (
            <Container className="App">
                <h1>Login</h1>
                <form onSubmit={this.handleSubmit}>
                    <label>Email
                        <br />
                        <input type="email" name="email" onChange={this.set('email')} />
                    </label>
                    <br />
                    <label>Password
                        <br />
                        <input type="password" name="password" onChange={this.set('password')} />
                    </label>
                    <br />
                    <input type="submit" value="submit" />
                </form>
            </Container>
        )
    }
}

export default withRouter(Login)