import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { Container } from 'reactstrap'

import "../App.css";

class Response extends String { json = () => JSON.parse(this) }

function mockFetch(url, { body }) {
    const { restoname, phone, city, state, address } = body
    if (restoname == null || phone == null || city == null || state == null || address == null)
        return Promise.reject("field tidak boleh kosong!")

    return Promise.resolve(
        new Response(
            JSON.stringify({ accessToken: 'ini-token' })
        )
    )
}

class CreateNewResto extends Component {
    state = {
        restoname: null,
        phone: null,
        city: null,
        state: null,
        address: null,
    }
    set = name => event => this.setState({ [name]: event.target.value })
    handleSubmit = event => {
        event.preventDefault()
        const { restoname, phone, city, state, address } = this.state
        const { history } = this.props
        mockFetch('http://api.expample.com/v1/auth/login', {
            body: { restoname, phone, city, state, address }
        })
            .then(response => response.json())
            .then(json => {
                localStorage.setItem('token', json.accessToken)
                alert("Registrasi sukses!")
                alert(`Resto: ${restoname}, Phone: ${phone}, City: ${city}, State: ${state}, Address: ${address}`)
                history.push('/')
            })
            .catch(err => { alert(err) }
            )
    }

    render() {
        return (
            <Container className="App">
                <h1>Create new Resto</h1>
                <form onSubmit={this.handleSubmit}>
                    <label>Resto name:
                        <br />
                        <input type="text" name="resto-name" onChange={this.set('restoname')} />
                    </label>
                    <br />
                    <label>Phone Number:
                        <br />
                        <input type="tel" name="phone" onChange={this.set('phone')} />
                    </label>
                    <br />
                    <label>City:
                        <br />
                        <input type="text" name="city" onChange={this.set('city')} />
                    </label>
                    <br />
                    <label>State:
                        <br />
                        <input type="text" name="state" onChange={this.set('state')} />
                    </label>
                    <br />
                    <label>Address:
                        <br />
                        <input type="text" name="address" onChange={this.set('address')} />
                    </label>
                    <br />

                    <input type="submit" value="submit" />
                </form>
            </Container>
        )
    }
}

export default withRouter(CreateNewResto)