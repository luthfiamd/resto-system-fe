import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import Home from './pages/Home'
import Login from './pages/Login'
import SignUp from './pages/SignUp'
import CreateNewResto from './pages/CreateNewResto'

import NotFound from './pages/NotFound'

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/:id" component={Home} />
          <Route exact path="/user-resto/login" component={Login} />
          <Route exact path="/user-resto/register" component={SignUp} />
          <Route exact path="/resto/register" component={CreateNewResto} />
          <Route component={NotFound} />
        </Switch>
      </Router>
    )
  }
}

export default App;
